const express = require('express')
const server = express()

const PORT = process.env.PORT || 5000

const bodyParser = require('body-parser')
server.use(bodyParser.json())

const request = require('superagent')
const nodemailer = require('nodemailer')

server.use(express.static(__dirname + '/public'));

// Static file routes
server.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html')
})

server.get('/expeditions', (req, res) => {
  res.sendFile(__dirname + '/public/expeditions.html')
})

server.get('/media', (req, res) => {
  res.sendFile(__dirname + '/public/media.html')
})

server.get('/matt', (req, res) => {
  res.sendFile(__dirname + '/public/matt.html')
})

server.post('/sendmail', (req, res) => {
  var {name, subject, email, message, captcha} = req.body

  request
  .post(`https://www.google.com/recaptcha/api/siteverify?secret=6LeGaFAUAAAAAOL6pAfzaDEniaTxnzzT5_IxWSXH&response=${captcha}`)
  .end((err, response) => {
    if(err){
      res.status(404).send('Something went wrong')
    }
    if(response.body.success){
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'climb7mailer@gmail.com',
          pass: 'C1rcadence$'
        }
      })

      var mailOptions = {
        from: 'climb7mailer@gmail.com',
        to: 'matt@climb7.com, mike@climb7.com',
        subject: `Climb7.com: ${subject}`,
        text: `From: ${name} - ${email} \n\n ${message}`
      }

      transporter.sendMail(mailOptions, function(error, info) {
        if(error){
          console.log(error);
          res.status(503).send('Something went wrong on our end. Please try again later')
        }

        console.log('Email sent ', info.response);
        res.status(200).send('Mail sent')
      })
    }else{
      res.status(401).send('You are a robot')
    }
  })
})

server.listen(PORT, () => {
  console.log('Server listening on port', PORT);
})
