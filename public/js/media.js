(function(){
  $(".lightgallery").lightGallery();
  $("img").unveil(100);


  $(window).resize(() => {
    var width = window.innerWidth

    if(width <= 860){
      console.log('smaller');
      $('.galleryimg').addClass('smaller')
    }else if(width > 860){
      $('.galleryimg').removeClass('smaller')
    }
  })

}())
