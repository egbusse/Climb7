(function() {
  localStorage.clear()
  var container = $('#instagram')
  var col1 = $('#col1')
  var col2 = $('#col2')

  const fullDataLink = 'https://api.instagram.com/oembed?url=http://instagr.am/p/'

  var re = /sharedData = .+;/

  $.get('https://www.instagram.com/matt_moniz/', (data) => {
    var sharedData = data.match(re)[0]

    sharedData = sharedData.replace('sharedData = ', '')
    sharedData = sharedData.replace(';', '')
    sharedData = JSON.parse(sharedData)

    sharedData.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges.forEach((picture) => {
      shortLinks.push({
        code: picture.node.shortcode,
        timestamp: picture.node.taken_at_timestamp,
        html: null
      });
    })

    function hitEndpoint(item) {
      return new Promise((resolve, reject) => {
        $.get(`${fullDataLink}${item.code}/`, (response) => {
          item.html = response.html
          resolve(item)
        })
      })
    }

    async function getResponses(array) {
      var promises = array.map(hitEndpoint)

      await Promise.all(promises).then((values) => {
        promises = values
      })

      var sorted = promises.sort((a, b) => {
        return b.timestamp - a.timestamp
      })

      sorted.forEach((picture) => {
        $('#instagram-embed').append(`<div class="col s12">${picture.html}</div>`)
      })

      $('#insta_preview').append(`<div class="col s4">${sorted[0].html}</div>`)
      $('#insta_preview').append(`<div class="col s4">${sorted[1].html}</div>`)
      $('#insta_preview').append(`<div class="col s4">${sorted[2].html}</div>`)
    }

    getResponses(shortLinks)
  })

  // handle social media popout
  var expanded = false
  var shortLinks = []

  var socialMedia = {
    'facebook_icon': {
      object: $('#facebook_icon, a'),
      embed: $('#facebook-embed')
    },
    'twitter_icon': {
      object: $('#twitter_icon'),
      embed: $('#twitter-embed')
    },
    'instagram_icon': {
      object: $('#instagram_icon'),
      embed: $('#instagram-embed')
    }
  }

  $('.icon-container').on('click', (event) => {
    let target = socialMedia[$(event.target)[0].id]

    if($(event.target)[0].id === 'facebook_icon'){
      return
    }

    if(expanded === true){
      expanded = false;
      $('#social-content').animate({
        left: -510
      }, 200)
      $('.fixed-right').animate({
        left: 0
      }, 500)

      for(var media in socialMedia){
          if(media !== event.target.id){
            socialMedia[media].object.parent().slideDown(200)
          }
      }

      return
    }

    expanded = true

    for(var media in socialMedia){
        if(media !== event.target.id){
          socialMedia[media].object.parent('.icon-container').slideUp(200)
        }else{
          $('#social-content').children().hide()
          socialMedia[media].embed.show()
        }
    }

    $('.fixed-right').animate({
      left: 500
    }, 500)
    $('#social-content').animate({
      left: 0
    }, 500)
  })

  var out = false

  $(window).on('scroll', () => {
    let scroll = this.scrollY
    let prompted = localStorage.getItem('prompted') == 'true';

    if(scroll > 500 && out === false){
      out = true
      $('.fixed-right').animate({
        left: 0
      }, 500)
      if(!prompted){
        $('#fixed-footer').fadeIn(800)
        localStorage.setItem('prompted', true)
        setTimeout(() => {
          $('#fixed-footer').fadeOut(1200)
        }, 5000)
      }

    }else if(scroll < 500 && out === true){
      out = false;
      expanded = false;
      $('.fixed-right').animate({
        left: -80
      }, 500)
      $('#social-content').animate({
        left: -510
      }, 200)
      $('#fixed-footer').fadeOut(300)

      for(var media in socialMedia){
          if(media !== event.target.id){
            socialMedia[media].object.parent().slideDown(200)
          }
      }
    }
  })

})()
