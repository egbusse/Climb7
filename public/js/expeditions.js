(function() {
  //handle url params for link parsing
  var params = location.search.substring(1).split(/[=&]/)
  var paramsObj = {}
  if(params.length % 2 === 0) {
    for(var i = 0; i <= (params.length)/2; i+=2){
      paramsObj[params[i]] = params[i+1]
    }
  }

  $('.carousel.carousel-slider').carousel({fullWidth: true, indicators: true});
  $('.collapsible').collapsible();
  $(".dropdown-button").dropdown()

  rotateCarousel() // auto rotate carousel
  function rotateCarousel() {
    $('.carousel').carousel('next')
    setTimeout(rotateCarousel, 4500)
  }

  setCategory((paramsObj.section ? paramsObj.section : 'sevensummits'))

  // Set Denali as active mountain
  $('.exp-info').children().hide()
  $((paramsObj.summit ? `#${paramsObj.summit}` : '#everest')).show()

  $(".button-collapse").sideNav(); //mobile nav support
}())

var lastCategory;

function setCategory(category) {
  if (lastCategory === category){
    return;
  }

  lastCategory = category

  $('#fifty, #sevensummits, #fourteen').fadeOut(300)
  $(`#${category}`).delay(350).fadeIn(300)

  if (category === 'fifty') {
    $('#cesiumContainer').hide()
    $('#googlemap').show()
    initMap()
  } else if (category === 'fourteen') {
    $('#cesiumContainer').hide()
    $('#googlemap').show()
    initMap('fourteen')
  } else if (category === 'sevensummits') {
    $('#googlemap').hide()
    $('#cesiumContainer').show()
  }
}


function setPage(page) {
  // handle pagination hightlight
  $('.pagination').children().removeClass('active')
  $(event.target).parent().addClass('active')

  var pageid = `#${page}`
  changeMap(page)
  $('.exp-info').children().hide()
  $(pageid).show()

  window.history.replaceState(null, null, `/expeditions?summit=${page}`)
}

function initMap(category='fifty') {
  $('#googlemap').empty()

  var united_states = {
    lat: 40.115934,
    lng: -105.615844
  };

  if(category === 'fourteen'){
    // render map for fourteen expedition
    var map = new google.maps.Map(document.getElementById('googlemap'), {
      zoom: 9,
      center: {
        lat: 39.224713,
        lng: -106.001962
      }
    });

    var markers = {}
    var infowindows = {}
    pathCoords = []
    var fourteenPeaks = {
      'Torreys Peak': {
        lat: 39.642878,
        lng: -105.821420
      },
      'Greys Peak': {
        lat: 39.633609,
        lng: -105.817139
      },
      'Mt. Democrat': {
        lat: 39.339642,
        lng: -106.139953
      },
      'Mt. Lincoln': {
        lat: 39.351543,
        lng: -106.111409
      },
      'Mt. Bross': {
        lat: 39.335538,
        lng: -106.107486
      },
      'Tabegauche Peak': {
        lat: 38.625275,
        lng: -106.250855
      },
      'Mt. Shavano': {
        lat: 38.618886,
        lng: -106.239466
      },
      'Mt. Antero': {
        lat: 38.673886,
        lng: -106.246688
      },
      'Mt. Missouri': {
        lat: 38.947216,
        lng: -106.378358
      },
      'Mt. Belford': {
        lat: 38.960549,
        lng: -106.361135
      },
      'Mt. Oxford': {
        lat: 38.964716,
        lng: -106.338913
      },
      'Mt. Elbert': {
        lat: 39.117816,
        lng: -106.445231
      },
      'Mt. Bierstadt': {
        lat: 39.582765,
        lng: -105.668615
      },
      'Mt, Evans': {
        lat: 39.588301,
        lng: -105.643829
      },
    }

    for (var peak in fourteenPeaks) {
      markers[peak] = new google.maps.Marker({
        position: {
          lat: fourteenPeaks[peak].lat,
          lng: fourteenPeaks[peak].lng,
        },
        map: map,
        title: peak,
        infowindow: new google.maps.InfoWindow({
          content: `${peak}`
        })
      })

      google.maps.event.addListener(markers[peak], 'click', function() {
          this.infowindow.open(map, this);
          setTimeout(() => {
            this.infowindow.close()
          }, 5000)
      });

      pathCoords.push({
        lat: fourteenPeaks[peak].lat,
        lng: fourteenPeaks[peak].lng,
      })
    }

    // Add path to map
    var flightPath = new google.maps.Polyline({
        path: pathCoords,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    flightPath.setMap(map);
  }else{
    // render map for 50 states expedition
    var map = new google.maps.Map(document.getElementById('googlemap'), {
      zoom: 3,
      center: united_states
    });

    var peakinfo = {
      'Cheaha Mountain': {
        'state': 'Alabama',
        'lat': 33.4857,
        'lng': -85.8091,
        'elev': '2,405'
      },
      'Denali': {
        'state': 'Alaska',
        'lat': 63.0694,
        'lng': -151.004,
        'elev': '20,320'
      },
      'Humphreys Peak': {
        'state': 'Arizona',
        'lat': 35.3333,
        'lng': -111.683,
        'elev': '12,633'
      },
      'Mount Magazine': {
        'state': 'Arkansas',
        'lat': 35.1667,
        'lng': -93.644698,
        'elev': '2,753'
      },
      'Mount Whitney': {
        'state': 'California',
        'lat': 36.578523,
        'lng': -118.292327,
        'elev': '14,494'
      },
      'Mount Elbert': {
        'state': 'Colorado',
        'lat': 39.1178,
        'lng': -106.445,
        'elev': '14,433'
      },
      'Mount Frissell': {
        'state': 'Connecticut/Massachusetts',
        'lat': 42.051083,
        'lng': -73.481972,
        'elev': '2,380'
      },
      'Ebright Azimuth': {
        'state': 'Delaware',
        'lat': 39.836476,
        'lng': -75.520592,
        'elev': 442
      },
      'Britton Hill': {
        'state': 'Florida',
        'lat': 30.987719,
        'lng': -86.281867,
        'elev': 345
      },
      'Brasstown Bald': {
        'state': 'Georgia',
        'lat': 34.8723,
        'lng': -83.8099,
        'elev': '4,784'
      },
      'Mauna Kea': {
        'state': 'Hawaii',
        'lat': 19.8236,
        'lng': -155.471,
        'elev': '13,796'
      },
      'Borah Peak (Mount Borah)': {
        'state': 'Idaho',
        'lat': 44.1333,
        'lng': -113.783,
        'elev': '12,662'
      },
      'Charles Mound': {
        'state': 'Illinois',
        'lat': 42.504146,
        'lng': -90.239765,
        'elev': '1,235'
      },
      'Hoosier Hill': {
        'state': 'Indiana',
        'lat': 40.000345,
        'lng': -84.850884,
        'elev': '1,257'
      },
      'Hawkeye Point': {
        'state': 'Iowa',
        'lat': 43.460154,
        'lng': -95.708891,
        'elev': '1,670'
      },
      'Mount Sunflower': {
        'state': 'Kansas',
        'lat': 39.021982,
        'lng': -102.037141,
        'elev': '4,039'
      },
      'Black Mountain': {
        'state': 'Kentucky',
        'lat': 36.9167,
        'lng': -82.9,
        'elev': '4,145'
      },
      'Driskill Mountain': {
        'state': 'Louisiana',
        'lat': 32.424856,
        'lng': -92.89669,
        'elev': 535
      },
      'Mount Katahdin': {
        'state': 'Maine',
        'lat': 45.9,
        'lng': -68.9167,
        'elev': '5,268'
      },
      'Backbone Mountain': {
        'state': 'Maryland',
        'lat': 39.2333,
        'lng': -79.4833,
        'elev': '3,360'
      },
      'Mount Greylock': {
        'state': 'Massachusetts',
        'lat': 42.6376,
        'lng': -73.1662,
        'elev': '3,491'
      },
      'Mount Arvon': {
        'state': 'Michigan',
        'lat': 46.7555,
        'lng': -88.156,
        'elev': '1,979'
      },
      'Eagle Mountain': {
        'state': 'Minnesota',
        'lat': 47.8974,
        'lng': -90.5601,
        'elev': '2,301'
      },
      'Woodall Mountain': {
        'state': 'Mississippi',
        'lat': 34.787745,
        'lng': -88.241625,
        'elev': 806
      },
      'Taum Sauk': {
        'state': 'Missouri',
        'lat': 37.571301,
        'lng': -90.729144,
        'elev': '1,779'
      },
      'Granite Peak': {
        'state': 'Montana',
        'lat': 45.1667,
        'lng': -109.8,
        'elev': '12,799'
      },
      'Panorama Point': {
        'state': 'Nebraska',
        'lat': 41.007235,
        'lng': -104.031264,
        'elev': '5,426'
      },
      'Boundary Peak': {
        'state': 'Nevada',
        'lat': 37.85,
        'lng': -118.35,
        'elev': '13,143'
      },
      'Mount Washington': {
        'state': 'New Hampshire',
        'lat': 44.2706,
        'lng': -71.3047,
        'elev': '6,288'
      },
      'High Point': {
        'state': 'New Jersey',
        'lat': 41.320948,
        'lng': -74.661562,
        'elev': '1,803'
      },
      'Wheeler Peak': {
        'state': 'New Mexico',
        'lat': 36.556941,
        'lng': -105.416901,
        'elev': '13,161'
      },
      'Mount Marcy': {
        'state': 'New York',
        'lat': 44.1125,
        'lng': -73.9239,
        'elev': '5,344'
      },
      'Mount Mitchell': {
        'state': 'North Carolina',
        'lat': 35.7647,
        'lng': -82.2653,
        'elev': '6,684'
      },
      'White Butte': {
        'state': 'North Dakota',
        'lat': 46.386965,
        'lng': -103.302694,
        'elev': '3,506'
      },
      'Campbell Hill': {
        'state': 'Ohio',
        'lat': 40.370328,
        'lng': -83.720089,
        'elev': '1,549'
      },
      'Black Mesa': {
        'state': 'Oklahoma',
        'lat': 36.931838,
        'lng': -102.997859,
        'elev': '4,973'
      },
      'Mount Hood': {
        'state': 'Oregon',
        'lat': 45.373403,
        'lng': -121.695642,
        'elev': '11,237'
      },
      'Mount Davis': {
        'state': 'Pennsylvania',
        'lat': 39.7861,
        'lng': -79.1761,
        'elev': '3,213'
      },
      'Jerimoth Hill': {
        'state': 'Rhode Island',
        'lat': 41.849603,
        'lng': -71.778896,
        'elev': 812
      },
      'Sassafras Mountain': {
        'state': 'South Carolina',
        'lat': 35.065146,
        'lng': -82.777349,
        'elev': '3,560'
      },
      'Harney Peak': {
        'state': 'South Dakota',
        'lat': 43.8667,
        'lng': -103.533,
        'elev': '7,242'
      },
      "Clingman's Dome": {
        'state': 'Tennessee/North Carolina',
        'lat': 35.55,
        'lng': -83.5,
        'elev': '6,643'
      },
      'Guadalupe Peak': {
        'state': 'Texas',
        'lat': 31.8833,
        'lng': -104.867,
        'elev': '8,749'
      },
      'Kings Peak': {
        'state': 'Utah',
        'lat': 40.7833,
        'lng': -110.367,
        'elev': '13,528'
      },
      'Mount Mansfield': {
        'state': 'Vermont',
        'lat': 44.5436,
        'lng': -72.8147,
        'elev': '4,393'
      },
      'Mount Rogers': {
        'state': 'Virginia',
        'lat': 36.6597,
        'lng': -81.5447,
        'elev': '5,729'
      },
      'Mount Rainier': {
        'state': 'Washington',
        'lat': 46.8528,
        'lng': -121.759,
        'elev': '14,411'
      },
      'Spruce Knob': {
        'state': 'West Virginia',
        'lat': 38.7,
        'lng': -79.532778,
        'elev': '4,863'
      },
      'Timms Hill': {
        'state': 'Wisconsin',
        'lat': 45.4511,
        'lng': -90.1951,
        'elev': '1,952'
      },
      'Gannett Peak': {
        'state': 'Wyoming',
        'lat': 43.1844,
        'lng': -109.653,
        'elev': '13,804'
      }
    }

    var markers = {}
    var infowindows = {}

    for (var peak in peakinfo) {
      markers[peak] = new google.maps.Marker({
        position: {
          lat: peakinfo[peak].lat,
          lng: peakinfo[peak].lng,
        },
        map: map,
        title: peak,
        infowindow: new google.maps.InfoWindow({
          content: `${peak}, ${peakinfo[peak].state}. ${peakinfo[peak].elev}ft`
        })
      })

      google.maps.event.addListener(markers[peak], 'click', function() {
          this.infowindow.open(map, this);
          setTimeout(() => {
            this.infowindow.close()
          }, 5000)
      });
    }
  }
}
