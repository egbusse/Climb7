var viewer = undefined;

(function () {
  var params = location.search.substring(1).split(/[=&]/)
  var paramsObj = {}
  if(params.length % 2 === 0) {
    for(var i = 0; i <= (params.length)/2; i+=2){
      paramsObj[params[i]] = params[i+1]
    }
  }

  Cesium.BingMapsApi.defaultKey = 'Agc1OTM8oPm7WtbcEiuKHsKP2LZq1tHs_R4FYDR_eeeMKUTXW4OaN5CHv3bDCeAD'

  viewer = new Cesium.Viewer('cesiumContainer', {
    fullscreenButton: false,
    timeline: false,
    sceneModePicker: false,
    homeButton: false,
    baseLayerPicker: false,
    geocoder: false,
    animation: false,
  });

  // Enable 3D terrain on load
  var terrainProvider = new Cesium.CesiumTerrainProvider({
    url : '//assets.agi.com/stk-terrain/world'
  });
  viewer.terrainProvider = terrainProvider;

  // fly to first mountain: Everest
  setTimeout(() => {
    var initialLocation = (paramsObj.summit ? paramsObj.summit : 'everest')
    viewer.camera.flyTo({
      destination: geocoords[initialLocation].destination,
      orientation: geocoords[initialLocation].orientation
    })
  }, 2000)

}())

var geocoords = {
  everest: {
    destination: new Cesium.Cartesian3(309788.32099496963, 5633699.0609035725, 2984834.8698092536),
    orientation: {
      heading : Cesium.Math.toRadians(120),
      pitch : Cesium.Math.toRadians(-30),
      roll : 0.0
    }
  },
  denali: {
    destination: new Cesium.Cartesian3(-2543360.696223656, -1402602.0538997983, 5665996.451509615),
    orientation: {
      heading : Cesium.Math.toRadians(40),
      pitch : Cesium.Math.toRadians(-18),
      roll : 0.0
    }
  },
  elbrus: {
    destination: new Cesium.Cartesian3(3437801.0091027166, 3145810.1272225883, 4348852.2384855),
    orientation: {
      heading : Cesium.Math.toRadians(-5),
      pitch : Cesium.Math.toRadians(-18),
      roll : 0.0
    }
  },
  aconcagua: {
    destination: new Cesium.Cartesian3(1831256.7849244715, -5058476.608112964, -3426433.6882897187),
    orientation: {
      heading : Cesium.Math.toRadians(83),
      pitch : Cesium.Math.toRadians(-18),
      roll : 0.0
    }
  },
  kilimanjaro: {
    destination: new Cesium.Cartesian3(5063484.827800879, 3874061.1767346943, -332861.73058861314),
    orientation: {
      heading : Cesium.Math.toRadians(-130),
      pitch : Cesium.Math.toRadians(-18),
      roll : 0.0
    }
  },
  vinson: {
    destination: new Cesium.Cartesian3(95514.8020095069, -1249910.886314521, -6237851.213834994),
    orientation: {
      heading : Cesium.Math.toRadians(0),
      pitch : Cesium.Math.toRadians(-18),
      roll : 0.0
    }
  },
  carstensz: {
    destination: new Cesium.Cartesian3(-4659434.264231708, 4338098.750527547, -451778.3038573424),
    orientation: {
      heading : Cesium.Math.toRadians(-5),
      pitch : Cesium.Math.toRadians(-10),
      roll : 0.0
    }
  }
}

function changeMap(mountain) {
  if(viewer) {
    viewer.camera.flyTo({
      destination: geocoords[mountain].destination,
      orientation: geocoords[mountain].orientation
    })
  }else{
    console.log('viewer was unable to load');
  }
}

function getPosition() {
  console.log('getting position');
  console.log(viewer.camera.position);
}
