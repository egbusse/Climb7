(function() {
  $('ul.tabs').tabs();  // Initialize Tabs
  $('.parallax').parallax(); // start parallax
  $('.carousel').carousel({
    dist: -25,
    padding: 10,
  }); // start carousel

  $(".button-collapse").sideNav(); //mobile nav support

  // rotateCarousel() // auto rotate carousel
  function rotateCarousel() {
    $('.carousel').carousel('next')
    console.log($('.carousel-item.active'));
    setTimeout(rotateCarousel, 4500)
  }

  $('#expand_profile').on('click', (event) => {
    $('#hidden_profile').slideToggle(200)
  })

  // 7summit section animations
  $('.mtn-container').on('click', (event) => {
    if($(event.target).parent().hasClass('mtn-active')){
      $('.mtn-container').removeClass('mtn-active')
    }else{
      $('.mtn-container').removeClass('mtn-active')
      $(event.target).parent().addClass('mtn-active')
    }
  })

  $('#summit-button').on('click', (event) => {
    $('#summits').slideToggle(300)
  })

  var highlights = ['#natgeo', '#eaglescouts','#mensjournal', '#cnn', '#outaward', '#makalu', '#honormedal', '#broncos']
  var currIndex = 0
  $('.plx-right').children().hide()
  $('.plx-right').children('#natgeo').fadeIn(500)
  setInterval(changeHighlight, 7000)
  function changeHighlight() {
    $(highlights[currIndex]).fadeOut(1100, () => {
      if(currIndex === highlights.length - 1){
        currIndex = 0
      }else{
        currIndex++
      }

      $(highlights[currIndex]).fadeIn(500)
    })
  }

  var opacity = 0;
  var baseScroll = window.scrollY
  var lastPos = window.scrollY
  $mainNav = $('#main-nav')

  // $(window).on('scroll', () => {
  //   let scroll = this.scrollY
  //   determineNavOpacity(scroll, lastPos, opacity)
  // })
  //
  // function determineNavOpacity(scrolled, last) {
  //   var logoHeight = $('#nav-logo').height()
  //
  //   if(scrolled > last && scrolled <= 500){
  //     if(opacity < 0.5){
  //       opacity += 0.015
  //       if(logoHeight >= 52){
  //         var newHeight = logoHeight -= 2
  //         $('#nav-logo').height(newHeight)
  //       }
  //     }
  //     lastPos = scrolled
  //     $mainNav.css({
  //       background: `rgba(0, 0, 0, ${opacity})`
  //     })
  //   }else if(scroll < last && scroll <= 500){
  //     if(opacity >= 0){
  //       opacity -= 0.015
  //       if(logoHeight <= 98){
  //         var newHeight = logoHeight += 2
  //         $('#nav-logo').height(newHeight)
  //       }
  //     }
  //     lastPos = scrolled
  //     $mainNav.css({
  //       background: `rgba(0, 0, 0, ${opacity})`
  //     })
  //   }
  //
  //   if(scroll === 0) {
  //     $mainNav.css({
  //       background: `rgba(0, 0, 0, 0)`
  //     })
  //     $('#nav-logo').height(100)
  //   }else if(scroll >= 500) {
  //     $mainNav.css({
  //       background: `rgba(0, 0, 0, 0.5)`
  //     })
  //     $('#nav-logo').height(50)
  //   }
  // }

  // Get and handle weather data
  // var everest_weather = localStorage.getItem('everest_weather')
  // const date = new Date()
  //
  // if(!everest_weather){
  //   $.get('http://api.wunderground.com/api/ce332a451983956e/conditions/q/NP/lola.json', (response) => {
  //     response['timestamp'] = date.getTime()
  //     localStorage.setItem('everest_weather', JSON.stringify(response))
  //     populateWeather()
  //   })
  // }else{
  //   var delta = date.getTime() - JSON.parse(localStorage.getItem('everest_weather')).timestamp
  //   if(delta > 86400000){
  //     $.get('http://api.wunderground.com/api/ce332a451983956e/conditions/q/NP/lola.json', (response) => {
  //       response['timestamp'] = date.getTime()
  //       localStorage.setItem('everest_weather', JSON.stringify(response))
  //       populateWeather()
  //     })
  //   }else{
  //     populateWeather()
  //   }
  // }
  //
  // function populateWeather() {
  //   var weatherData = JSON.parse(localStorage.getItem('everest_weather')).current_observation
  //   console.log(weatherData);
  //
  //   $('#weather').append(`<span>`)
  // }

}())

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function handleMessageSubmit() {
  var name = $('#input_name').val()
  var email = $('#input_email').val()
  var subject = $('#input_subject').val()
  var message = $('#message_content').val()

  var captcha = grecaptcha.getResponse()

  if(!name || !subject || !message || !email){
    alert('Invalid or missing field inputs')
    return
  }

  if(!validateEmail(email)){
    alert('Invalid email address')
    return
  }

  if(!captcha) {
    alert('Please confirm that you\'re not a robot')
    return
  }

  $('#contactForm').empty().append(`<center><div class="preloader-wrapper active">
    <div class="spinner-layer spinner-red-only">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div><div class="gap-patch">
        <div class="circle"></div>
      </div><div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div></center>`)

  $.ajax({
    url: './sendmail',
    type: 'post',
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify({
      name: name,
      subject: subject,
      message: message,
      captcha: captcha,
      email: email
    }),
    error: function (data) {
      console.log(data);
      $('#contactForm').empty().append(`<center><h5 style="color: white">Your message has been sent!</h5></center>`)
    }
  })
}

function toggleTracking() {
  if($('#tracking').is(':visible')){
    $('#tracking').slideUp(500)
  }else{
    $('#tracking').slideDown(500)
  }
}

function scrollToSection(section){
  var section_obj = $(`#${section}`)
  var offset = section_obj.offset().top

  window.scroll({
    top: offset,
    left: 0,
    behavior: 'smooth'
  })
}
